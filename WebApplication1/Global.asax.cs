﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Hangfire;
using System.Data.Entity;
using WebApplication1.Repositories;
using WebApplication1.libs;

namespace WebApplication1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private BackgroundJobServer _backgroundJobServer;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            System.Web.Http.GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //mandar clase para envio de correo
            JobScheduler.Start();

            //var db = new HangfireContext();
            //Hangfire.GlobalConfiguration.Configuration
            //    .UseSqlServerStorage(db.Database.Connection.ConnectionString);
            Hangfire.GlobalConfiguration.Configuration.UseSqlServerStorage("HangfireContext");
            _backgroundJobServer = new BackgroundJobServer();

            //BackgroundJob.Enqueue(() => Console.WriteLine("Fire-and-forget Job Executed"));
            //ValidarDocumentos validarDoc = new ValidarDocumentos();

            ////lanzar servicio periodico
            //RecurringJob.AddOrUpdate(() => validarDoc.iniciar(), Cron.MinuteInterval(30));


        }
    }

    public class HangfireContext : DbContext
    {
        public HangfireContext() : base("HangfireContext")  // Remove "name="
        {
            Database.SetInitializer<HangfireContext>(null);
            Database.CreateIfNotExists();
        }
    }
}
